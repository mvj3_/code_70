*   《数据之美》Chapter.5 信息平台和数据科学家的兴起。Facebook抓取wikipedia的所有语言去生成character trigram frequency counts，以此来构造识别用户母语的分类器。

###     Ruby
*     https://github.com/mkdynamic/vss Simple vector space search engine 适合小数据
*     https://github.com/louismullie/graph-rank Ruby implementation of the PageRank and TextRank algorithms.
*     https://github.com/louismullie/treat Treat is a toolkit for natural language processing and computational linguistics in Ruby. The Treat project aims to build a language- and algorithm- agnostic NLP framework for Ruby with support for tasks such as document retrieval, text chunking, segmentation and tokenization, natural language parsing, part-of-speech tagging, keyword extraction and named entity recognition. 挺丰富的
*      https://github.com/cantino/ruby-readability   Ruby Readability is a tool for extracting the primary readable content of a webpage. It is a Ruby port of arc90's readability project.
*      https://github.com/opennorth/tf-idf-similarity Ruby Vector Space Model (VSM) with tf*idf weights 千条记录以上的性能需要提升
*      https://github.com/quix/linalg Ruby Linear Algebra Library. A Fortran-based linear algebra package.
*      https://github.com/github/linguist We use this library at GitHub to detect blob languages, highlight code, ignore binary files, suppress generated files in diffs and generate language breakdown graphs.

####    及时记录和分享的原因是为了更好的交流，我不是牛人，我在学习。


### 其他项目参考
*      http://code.google.com/p/python-data-mining-platform/ 国人写的
